import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, map, mergeMap, switchMap} from 'rxjs/operators';
import { environment } from '~/environments/environment';
import {
  FetchMoreProducts,
  FetchMoreProductsFailure,
  FetchMoreProductsSuccess,
  FetchProducts,
  FetchProductsFailure,
  FetchProductsSuccess, FetchRecentProducts, FetchRecentProductsFailure, FetchRecentProductsSuccess,
  ProductsActionTypes
} from '~/app/redux/actions/products.actions';
import {Product, ProductApiResults} from '~/app/models/product';
import {ShowSnackbar} from '~/app/redux/actions/snackbar.actions';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {ProductsService} from '~/app/services/products.service';

@Injectable()
export class ProductsEffects {
  @Effect()
  getProducts$: Observable<Action> = this.actions$.pipe(
    ofType<FetchProducts>(ProductsActionTypes.FetchProductAction),
    mergeMap(action =>
      this.productsService.fetch(action)
        .pipe(
          switchMap(
            (products: ProductApiResults) => {
              return [
                new FetchProductsSuccess(
                  products.docs.map(p => new Product(p)),
                  products.page,
                  products.pages,
                  products.total,
                )
              ];
            }
          ),
          catchError(err => {
            return of(new FetchProductsFailure());
          })
        )
    )
  );

  @Effect()
  getRecentProducts$: Observable<Action> = this.actions$.pipe(
    ofType<FetchRecentProducts>(ProductsActionTypes.FetchRecentProductsAction),
    mergeMap(action =>
      this.productsService.fetchRecent(action)
        .pipe(
          switchMap(
            (products: ProductApiResults) => {
              return [
                new FetchRecentProductsSuccess(
                  products.docs.map(p => new Product(p))
                )
              ];
            }
          ),
          catchError(err => {
            return of(new FetchRecentProductsFailure());
          })
        )
    )
  );

  @Effect()
  getProductsFailure$: Observable<Action> = this.actions$.pipe(
    ofType<FetchProducts>(
      ProductsActionTypes.FetchProductFailureAction
    ),
    map(
      action =>
        new ShowSnackbar(
          this.i18n.transform('snackbar.failedLoadingProducts'),
        )
    )
  );

  @Effect()
  getMoreProducts$: Observable<Action> = this.actions$.pipe(
    ofType<FetchMoreProducts>(ProductsActionTypes.FetchMoreProductAction),
    mergeMap(action =>
        this.productsService.fetch(action)
          .pipe(
            switchMap(
              (products: ProductApiResults) => {
                return [
                  new FetchMoreProductsSuccess(
                    products.docs.map(p => new Product(p)),
                    products.page,
                    products.pages,
                    products.total,
                  )
                ];
              }
            ),
            catchError(err => {
              return of(new FetchMoreProductsFailure());
            })
          )
    )
  );



  @Effect()
  getMoreProductsFailure$: Observable<Action> = this.actions$.pipe(
    ofType<FetchMoreProducts>(
      ProductsActionTypes.FetchMoreProductFailureAction
    ),
    map(
      action =>
        new ShowSnackbar(
          this.i18n.transform('snackbar.failedLoadingProducts'),
        )
    )
  );


  constructor(
    private actions$: Actions,
    private httpClient: HttpClient,
    private i18n: I18nPipe,
    private productsService: ProductsService
  ) {
  }
}
