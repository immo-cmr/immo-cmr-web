import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, select, Store} from '@ngrx/store';
import {defer, Observable, of} from 'rxjs';
import {catchError, filter, map, mergeMap, switchMap} from 'rxjs/operators';
import { environment } from '~/environments/environment';
import {
    AuthActionTypes,
    GetAuthFromLocalStorage,
    GetAuthFromLocalStorageFailure,
    Login,
    LoginFailure,
    LoginSuccess,
    Logout,
    LogoutFailure,
    LogoutSuccess
} from '~/app/redux/actions/auth.actions';
import {AuthResponse} from '~/app/models/user';
import {ShowSnackbar} from '~/app/redux/actions/snackbar.actions';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {LocalstorageService} from '~/app/services/localstorage.service';
import {AuthService} from '~/app/services/auth.service';
import {LoadFavourites} from '~/app/redux/actions/favourites.actions';
import {LoadSearches} from '~/app/redux/actions/search.actions';
import {FetchNotifications} from '~/app/redux/actions/notifications.actions';
import {AppState} from '~/app/redux/reducers';
import * as fromAuth from '~/app/redux/reducers/auth.reducer';
import {LoadConversations} from '~/app/redux/actions/conversation.actions';
import {PusherService} from '~/app/services/pusher.service';

@Injectable()
export class AuthEffects {
    @Effect()
    login$: Observable<Action> = this.actions$.pipe(
        ofType<Login>(AuthActionTypes.LoginAction),
        mergeMap(action =>
            this.httpClient
                .post(
                    `${environment.ApiBaseUrl}auth/login`,
                    action.credentials
                )
                .pipe(
                    map(
                        (user: AuthResponse) => {
                            return new LoginSuccess(user, action.afterLoginAction ? action.afterLoginAction : null);
                        }
                    ),
                    catchError(err => {
                      // console.log(err);
                      return of(new LoginFailure(err.error.message));
                    })
                )
        )
    );

    @Effect()
    LoginSuccess$: Observable<Action> = this.actions$.pipe(
        ofType<LoginSuccess>(
            AuthActionTypes.LoginSuccessAction
        ),
        switchMap(action => {
                this.localStorageService.setAuthUser(action.response as AuthResponse);
                // this.pusherService.connect(action.response.user);
                const actions: Action[] = [
                    // new FetchProducts(1),
                    new LoadFavourites(),
                    new LoadSearches(),
                    new FetchNotifications(),
                    new LoadConversations(),
                ];
                if (action.actionsToRun && action.actionsToRun.length) {
                    action.actionsToRun.forEach(a => actions.push(a));
                }
                if (!action.fromAppLanch) {
                    actions.push(
                        new ShowSnackbar(
                            this.i18n.transform('snackbar.loginSuccess'),
                        )
                    );
                }
                return actions;
            }
        )
    );

    @Effect()
    LoginFailure$: Observable<Action> = this.actions$.pipe(
        ofType<LoginFailure>(
            AuthActionTypes.LoginFailureAction
        ),
        mergeMap(action => {
                return of(
                    new ShowSnackbar(
                        this.i18n.transform('snackbar.serverError'),
                    )
                );
            }
        )
    );

    @Effect()
    logout$: Observable<Action> = this.actions$.pipe(
        ofType<Logout>(AuthActionTypes.LogoutAction),
        mergeMap(action =>
            this.httpClient
                .post(
                    `${environment.ApiBaseUrl}auth/logout`,
                    {
                        push_token: this.pushToken
                    }
                )
                .pipe(
                    map(
                        (user: any) => {
                            this.localStorageService.logout();
                            // this.pusherService.disconnect();
                            return new LogoutSuccess();
                        }
                    ),
                    catchError(err => {
                        console.log('LOG OUT ERROR ', err);
                        return of(new LogoutFailure());
                    })
                )
        )
    );

    @Effect()
    getAuthFromLocalStorage$: Observable<Action> = this.actions$.pipe(
        ofType(AuthActionTypes.GetAuthFromLocalStorageAction),
        filter(() => this.authService.isLoggedIn()),
        switchMap(action => {
            const authToken = this.localStorageService.getAuthToken();
            const user = this.localStorageService.getUserObject();
            // console.log(authToken);

            if (!authToken.length || !user) {
                throw Error('Not Authenticated.');
            }

            // console.log('We are in dude !');
            // this.firebaseService.registerForToken();
            return [
                new LoginSuccess({
                    user,
                    access_token: authToken
                }, [], true)
            ];
        }),
        catchError(() => of(new GetAuthFromLocalStorageFailure()))
    );

    // Should be your last effect
    @Effect()
    init$: Observable<Action> = defer(() => {
        return of(new GetAuthFromLocalStorage());
    });

    private pushToken: string = null;
    constructor(
        private actions$: Actions,
        private httpClient: HttpClient,
        private i18n: I18nPipe,
        private localStorageService: LocalstorageService,
        private authService: AuthService,
        private store: Store<AppState>,
        private pusherService: PusherService) {
        this.store.pipe(
            select(fromAuth.getAuthFeatureState)
        ).subscribe(auth => {
            this.pushToken = auth.pushToken;
        });
    }
}
