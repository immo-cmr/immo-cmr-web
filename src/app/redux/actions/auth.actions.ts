import {Action} from '@ngrx/store';
import {AuthRequirements, AuthResponse, User} from '~/app/models/user';

export enum AuthActionTypes {
    LoginAction = '[Auth] Login action',
    LoginSuccessAction = '[Auth] Login Success',
    LoginFailureAction = '[Auth] Login Failure',
    LogoutAction = '[Auth] Logout action',
    LogoutSuccessAction = '[Auth] Logout Success',
    LogoutFailureAction = '[Auth] Logout Failure',
    GetAuthFromLocalStorageAction = '[Auth] Get auth from localstorage',
    GetAuthFromLocalStorageFailureAction = '[Auth] Get auth from localstorage failure',
    ShowAuthForm = '[Auth] Show auth modal form',
}

export class Login implements Action {
    readonly type = AuthActionTypes.LoginAction;

    constructor(public credentials: AuthRequirements, public afterLoginAction?: Action[]) {}
}

export class LoginSuccess implements Action {
    readonly type = AuthActionTypes.LoginSuccessAction;

    constructor(public response: AuthResponse , public actionsToRun?: Action[], public fromAppLanch?: boolean) {}
}

export class LoginFailure implements Action {
    readonly type = AuthActionTypes.LoginFailureAction;

    constructor(public message = '') {}
}

export class Logout implements Action {
    readonly type = AuthActionTypes.LogoutAction;
}

export class LogoutSuccess implements Action {
    readonly type = AuthActionTypes.LogoutSuccessAction;
}

export class LogoutFailure implements Action {
    readonly type = AuthActionTypes.LogoutFailureAction;
}

export class GetAuthFromLocalStorage implements Action {
    readonly type = AuthActionTypes.GetAuthFromLocalStorageAction;
}

export class GetAuthFromLocalStorageFailure implements Action {
    readonly type = AuthActionTypes.GetAuthFromLocalStorageFailureAction;
}

export class ShowAuthModal implements Action {
    readonly type = AuthActionTypes.ShowAuthForm;
}

export type AuthActionsUnion =
    | Login
    | LoginSuccess
    | LoginFailure
    | Logout
    | LogoutSuccess
    | LogoutFailure
    | ShowAuthModal
    ;
