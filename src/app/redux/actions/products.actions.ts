import {Action} from '@ngrx/store';
import {Product} from '~/app/models/product';
import {SearchFilter} from '~/app/models/search';

export enum ProductsActionTypes {
    FetchProductAction = '[Products] Fetch products action',
    FetchProductSuccessAction = '[Products] Fetch products success action',
    FetchProductFailureAction = '[Products] Fetch products failure action',
    FetchRecentProductsAction = '[Products] Fetch recent products action',
    FetchRecentProductsSuccessAction = '[Products] Fetch recent products success action',
    FetchRecentProductsFailureAction = '[Products] Fetch recent products failure action',
    FetchMoreProductAction = '[Products] Fetch more products action',
    FetchMoreProductSuccessAction = '[Products] Fetch more products success action',
    FetchMoreProductFailureAction = '[Products] Fetch more products failure action',
}

export class FetchProducts implements Action {
    readonly type = ProductsActionTypes.FetchProductAction;

    constructor(public page?: number, public filter: SearchFilter = null) {}
}

export class FetchProductsSuccess implements Action {
    readonly type = ProductsActionTypes.FetchProductSuccessAction;

    constructor(public products: Product[], public page = 1, public pages = 0, public total = 0) {}
}

export class FetchProductsFailure implements Action {
    readonly type = ProductsActionTypes.FetchProductFailureAction;
}

export class FetchRecentProducts implements Action {
    readonly type = ProductsActionTypes.FetchRecentProductsAction;

    constructor(public limit = 5) {}
}

export class FetchRecentProductsSuccess implements Action {
    readonly type = ProductsActionTypes.FetchRecentProductsSuccessAction;

    constructor(public products: Product[]) {}
}

export class FetchRecentProductsFailure implements Action {
    readonly type = ProductsActionTypes.FetchRecentProductsFailureAction;
}

export class FetchMoreProducts implements Action {
    readonly type = ProductsActionTypes.FetchMoreProductAction;

    constructor(public page = 2, public filter: SearchFilter = null) {}
}

export class FetchMoreProductsSuccess implements Action {
    readonly type = ProductsActionTypes.FetchMoreProductSuccessAction;

    constructor(public products: Product[], public page = 2, public pages = 0, public total = 0) {}
}

export class FetchMoreProductsFailure implements Action {
    readonly type = ProductsActionTypes.FetchMoreProductFailureAction;
}

export type ProductsActionsUnion =
    | FetchProducts
    | FetchProductsSuccess
    | FetchProductsFailure
    | FetchMoreProducts
    | FetchMoreProductsSuccess
    | FetchMoreProductsFailure
    | FetchRecentProducts
    | FetchRecentProductsSuccess
    | FetchRecentProductsFailure
    ;
