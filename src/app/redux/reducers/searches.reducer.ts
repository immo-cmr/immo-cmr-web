import {createFeatureSelector} from '@ngrx/store';
import {SearchActionsUnion, SearchActionTypes} from '~/app/redux/actions/search.actions';
import {AuthActionsUnion, AuthActionTypes} from '~/app/redux/actions/auth.actions';
import {Product} from '~/app/models/product';
import {ProductsActionsUnion} from '~/app/redux/actions/products.actions';

export interface State {
  entities: Product[];
  page: number;
  pages: number;
  total: number;
}

export const initialState: State = {
  entities: [],
  page: 1,
  pages: 0,
  total: 0,
};

export function reducer(
  state = initialState,
  action:
    | SearchActionsUnion
    | AuthActionsUnion
    | ProductsActionsUnion
): State {

  switch (action.type) {
    case SearchActionTypes.RunSearchSuccessAction:
      return {
        ...state,
        entities: action.products,
        pages: action.pages,
        page: action.page,
        total: action.total
      };
    case SearchActionTypes.RunMoreSearchSuccessAction:
      return {
        ...state,
        entities: state.entities.concat(action.products),
        pages: action.pages,
        page: action.page,
        total: action.total
      };
    case SearchActionTypes.DeleteSearchSuccessAction:
      return {
        ...state,
        entities: state.entities.filter(s => s.id !== action.id)
      };
    case SearchActionTypes.RunSearchAction:
      return  {
        ...state,
        entities: []
      };
    case AuthActionTypes.LogoutAction:
      return {
        ...state,
        entities: []
      };
    default:
      return state;
  }
}

export const getSearchesFeatureState = createFeatureSelector<State>('searches');
