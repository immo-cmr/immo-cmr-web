import {createFeatureSelector, createSelector} from '@ngrx/store';
import {User} from '~/app/models/user';
import {AuthActionsUnion, AuthActionTypes} from '~/app/redux/actions/auth.actions';
import {UserActionsUnion, UserActionTypes} from '~/app/redux/actions/user.actions';

export interface State {
    user: User;
    pushToken: string;
}

export const initialState: State = {
    user: null,
    pushToken: null,
};

export function reducer(
    state = initialState,
    action:
        | AuthActionsUnion
        | UserActionsUnion
): State {

    switch (action.type) {
        case AuthActionTypes.LoginSuccessAction:
            return {
                ...state,
                user: new User(action.response.user)
            };
        case AuthActionTypes.LogoutAction:
            return {
                ...state,
                user: null,
                pushToken: null,
            };
        case UserActionTypes.UpdateSettingsSuccessAction:
            return {
                ...state,
                user: {
                    ...state.user,
                    settings: {
                        ...state.user.settings,
                        id: action.settings.id || state.user.settings.id,
                        activeSearch: action.settings.activeSearch || state.user.settings.activeSearch,
                        activeSearchRadius: action.settings.activeSearchRadius || state.user.settings.activeSearchRadius,
                        autoDeleteNotifications: action.settings.autoDeleteNotifications || state.user.settings.autoDeleteNotifications,
                    }
                }
            };
        case UserActionTypes.RegisterDeviceSuccess:
            return {
              ...state,
              pushToken: action.device.pushToken
            };
        default:
            return state;
    }
}

export const getAuthFeatureState = createFeatureSelector<State>('auth');
export const getAuthUserSettings = createSelector(
    getAuthFeatureState,
    (state) => {
        return state.user.settings;
    }
);
