import {APP_INITIALIZER, NgModule} from '@angular/core';
import {ServerModule, ServerTransferStateModule} from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
import {I18nService, setupI18nFactory} from '~/app/services/i18n.service';

@NgModule({
  imports: [
    AppModule,
    ServerModule,
    ModuleMapLoaderModule,
    ServerTransferStateModule
  ],
  providers: [
    I18nService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupI18nFactory,
      deps: [
        I18nService
      ],
      multi: true
    },
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule {}
