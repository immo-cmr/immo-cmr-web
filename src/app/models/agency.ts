import {Location, LocationFromApi} from '~/app/models/location';

export class AgencyFromApi {
    id: string;
    profile_picture: string;
    is_professional: boolean;
    location: LocationFromApi;
    website: string;
    email: string;
    name: string;
    posts_counts: number;
}

export class Agency {
    id: string;
    profilePicture: string;
    isProfessional: boolean;
    location: Location;
    website: string;
    email: string;
    name: string;
    postsCounts: number;

    constructor(ag: AgencyFromApi) {
        this.id = ag.id;
        this.email = ag.email;
        this.isProfessional = ag.is_professional;
        this.location = new Location(ag.location);
        this.postsCounts = ag.posts_counts;
        this.profilePicture = ag.profile_picture;
        this.website = ag.website;
        this.name = ag.name;
    }
}

export class AgencyApiResults {
    docs: AgencyFromApi[];
    total: number;
    limit: number;
    page: number;
    pages: number;
}



