import {LocationFromApi, Location} from '~/app/models/location';
import {User, UserFromApi} from '~/app/models/user';

export enum ProductOptionsEnum {
    REFERENCE = 'reference',
    CONSTRUCTION_YEAR = 'construction_year',
    SURFACE = 'surface',
    NB_ROOMS = 'nb_rooms',
    NB_GARAGES = 'nb_garages',
    NB_FLOORS = 'nb_floors',
    NB_WATTER_ROOMS = 'nb_bating_rooms',
    NB_TERRACES = 'nb_terraces'
}

export enum ProductTransactionTypesEnum {
    RENT = 'rent',
    SELL = 'sell',
}

export enum ProductTypesEnum {
    HOUSE = 'house',
    APARTMENT = 'apartment',
    LOAN = 'loan',
    GARAGE = 'garage',
    COMMERCE = 'commerce',
    OFFICE = 'office',
    STORE = 'store'
}

export enum ProductCommoditiesEnum {
    SWIMMING_POOL = 'swimming_pool',
    CAVE = 'cave',
    PARKING = 'parking',
    INTERPHONE = 'interphone',
    GARAGE = 'garage'
}

export class ProductOptionsFromApi {
    _id: string;
    reference: string;
    construction_year: string;
    surface: string;
    nb_rooms: number;
    nb_garages: number;
    nb_floors: number;
    nb_bating_rooms: number;
    nb_terraces: number;
}

export class ProductFromApi {
    id: string;
    price: number;
    transaction_type: string;
    type: string[];
    commodities?: string[];
    location: LocationFromApi;
    owner: UserFromApi;
    options: ProductOptionsFromApi;
    title: string;
    description: string;
    images: [string];
    createdAt: string;
    updatedAt: string;
}

export class ProductOptions {
    id: string;
    reference: string;
    construction_year: string;
    surface: string;
    nb_rooms: any;
    nb_garages: any;
    nb_floors: any;
    nb_bating_rooms: any;
    nb_terraces: any;
    constructor(op: ProductOptionsFromApi) {
        this.id = op._id || '';
        this.reference = op.reference || 'n/a';
        this.construction_year = op.construction_year || 'n/a';
        this.nb_floors = op.nb_floors || 'n/a';
        this.nb_garages = op.nb_garages || 'n/a';
        this.nb_rooms = op.nb_rooms || 'n/a';
        this.nb_terraces = op.nb_terraces || 'n/a';
        this.nb_bating_rooms = op.nb_bating_rooms || 'n/a';
        this.surface = op.surface || 'n/a';
    }
}

export class Product {
    id: string;
    price: number;
    transactionType: string;
    type: string[];
    commodities: string[];
    options: ProductOptions;
    location: Location;
    owner: User;
    title: string;
    description: string;
    images: [string];
    createdAt: string;
    updatedAt: string;
    isFavourite: boolean;

    constructor(prod: ProductFromApi) {
        this.id = prod.id;
        this.type = prod.type;
        this.title = prod.title;
        this.transactionType = prod.transaction_type;
        this.description = prod.description;
        this.price = prod.price;
        this.location = new Location(prod.location);
        this.owner = new User(prod.owner);
        this.options = new ProductOptions(prod.options);
        this.images = prod.images;
        this.commodities = prod.commodities;
        this.createdAt = prod.createdAt;
        this.updatedAt = prod.updatedAt;

        this.isFavourite = false;
    }
}

export class ProductApiResults {
    docs: ProductFromApi[];
    total: number;
    limit: number;
    page: number;
    pages: number;
}



