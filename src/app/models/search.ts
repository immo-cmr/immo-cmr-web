import {ProductCommoditiesEnum, ProductTransactionTypesEnum, ProductTypesEnum} from '~/app/models/product';

// tslint:disable-next-line:max-line-length
export const PropertyTypes = [ProductTypesEnum.HOUSE, ProductTypesEnum.APARTMENT, ProductTypesEnum.COMMERCE, ProductTypesEnum.LOAN, ProductTypesEnum.GARAGE, ProductTypesEnum.OFFICE, ProductTypesEnum.STORE];
// tslint:disable-next-line:max-line-length
export const CommodityTypes = [ProductCommoditiesEnum.CAVE, ProductCommoditiesEnum.GARAGE, ProductCommoditiesEnum.INTERPHONE, ProductCommoditiesEnum.PARKING, ProductCommoditiesEnum.SWIMMING_POOL];

export class SearchFilter {
    constructor(
        public column: string,
        public direction: string
    ) {
    }
}

export class SearchFromApi {
    _id?: string;
    name?: string;
    transaction_type?: ProductTransactionTypesEnum;
    type?: ProductTypesEnum[];
    price_min?: number;
    price_max?: number;
    nb_rooms?: any[];
    commodities?: ProductCommoditiesEnum[];
    locations?: string[];
    results_count?: number;

    constructor(s: Search) {
        this._id = s.id || null;
        this.name = s.name || '';
        this.transaction_type = s.transactionType;
        this.type = s.type;
        this.price_max = s.priceMax || null;
        this.price_min = s.priceMin || 0;
        this.commodities = s.commodities;
        this.locations = s.locations;
        this.results_count = s.resultsCount;
    }
}

export class Search {
    id?: string;
    name?: string;
    transactionType?: ProductTransactionTypesEnum;
    type?: ProductTypesEnum[];
    priceMin?: number;
    priceMax?: number;
    nbRooms?: any[];
    commodities?: ProductCommoditiesEnum[];
    locations?: string[];
    resultsCount?: number;

    constructor(s?: SearchFromApi) {
        this.id = s._id || null;
        this.name = s.name || '';
        this.transactionType = s.transaction_type /*|| ProductTransactionTypesEnum.RENT*/;
        this.type = s.type || [];
        this.priceMin = Number(s.price_min || 0);
        this.priceMax = s.price_max ? Number(s.price_max) : null;
        this.nbRooms = s.nb_rooms || [];
        this.commodities = s.commodities || [];
        this.locations = s.locations || [];
        this.resultsCount = s.results_count || 0;
    }

    public adapt() {
        return new SearchFromApi(this);
    }
}
