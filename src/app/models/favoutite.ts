import {Product, ProductFromApi} from '~/app/models/product';

export class FavouriteFromApi {
    id: string;
    product: ProductFromApi;
}

export class Favourite {
    id?: string;
    product: Product;

    constructor(favourite: FavouriteFromApi) {
        this.id = favourite.id || null;
        this.product = new Product(favourite.product);
    }
}
