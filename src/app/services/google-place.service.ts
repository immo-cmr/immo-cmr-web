import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from '~/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GooglePlaceService {

  // tslint:disable-next-line:max-line-length
  private autoCompleteUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=__INPUT__&key=${environment.googleAutoCompleteKey}&libraries=places&components=country:cm`;
  constructor(private http: HttpClient) {}

  search(typed): Observable<any> {
    return this.http.get(this.autoCompleteUrl.replace('__INPUT__', typed), {
      withCredentials: false
    });
  }
}
