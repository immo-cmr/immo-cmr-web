import {Injectable} from '@angular/core';
import { environment } from '~/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import { TransferState, makeStateKey } from '@angular/platform-browser';
import {catchError, tap} from 'rxjs/internal/operators';
import {FetchMoreProducts, FetchProducts, FetchRecentProducts} from '~/app/redux/actions/products.actions';

export const PRODUCTS_KEY = makeStateKey('products');

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
      private httpClient: HttpClient,
      private state: TransferState,
  ) { }

  public fetch(action: FetchProducts | FetchMoreProducts) {
    const products = this.state.get(PRODUCTS_KEY, null as any);
    /*if (products) {
      return of(products);
    }*/

    // console.log(environment);
    return this.httpClient
      .get(
        action.filter === null ?
          `${environment.ApiBaseUrl}product?page=${action.page}&limit=15` :
          `${environment.ApiBaseUrl}product?page=${action.page}&sort=${action.filter.column}&value=${action.filter.direction}&limit=15`
      )
      .pipe(
        tap((res) => this.state.set(PRODUCTS_KEY, res as any)),
        catchError(this.handleError('fetchProperties', []))
      );
  }

  public fetchRecent(action: FetchRecentProducts) {
    const products = this.state.get(PRODUCTS_KEY, null as any);
    /*if (products) {
      return of(products);
    }*/

    // console.log(environment);
    return this.httpClient
      .get(
          `${environment.ApiBaseUrl}product?page=1&limit=${action.limit || 5}`
      )
      .pipe(
        tap((res) => this.state.set(PRODUCTS_KEY, res as any)),
        catchError(this.handleError('fetchRecent', []))
      );
  }

  public fetchProductDetails(id: string, withSimilar = false): Observable<any> {
    return this.httpClient.get(
        `${environment.ApiBaseUrl}product/${id}?similar=${withSimilar}`
    );
  }

  public findProductsByOwner(id: string): Observable<any> {
    return this.httpClient.get(`
      ${environment.ApiBaseUrl}product/${id}/user
    `);
  }

  public store(params: FormData) {
    return this.httpClient.post(
      `${environment.ApiBaseUrl}product`,
      params,
      {
        headers: {
        }
      }
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      return of(result as T);
    };
  }
}
