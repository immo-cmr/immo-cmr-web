import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {Language} from '~/app/services/i18n.service';
import {AuthResponse, SettingsFromApi} from '~/app/models/user';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  constructor(
    @Inject(PLATFORM_ID) private platformId: any
  ) {
  }

  public reset() {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.clear();
    }
  }

  public getCurrentLang(): string {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem('immo.lang') || Language.FR;
    }
  }

  public setCurrentLanguage(lang: Language): void {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem('immo.lang', lang);
    }
  }

  public getAuthToken() {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem('immo.token') || null;
    }
  }

  public setAuthUser(data: AuthResponse) {
    if (!data || !data.access_token || !data.user) {
      return false;
    }

    if (isPlatformBrowser(this.platformId)) {
      const user = JSON.stringify(data.user);
      localStorage.setItem('immo.user', user);
      localStorage.setItem('immo.token', data.access_token);
    }
  }

  public logout() {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.removeItem('immo.user');
      localStorage.removeItem('immo.token');
    }
  }

  public getUserObject() {
    if (isPlatformBrowser(this.platformId)) {

      const u = localStorage.getItem('immo.user') || null;
      if (u) {
        return JSON.parse(u);
      }
      return null;
    }
  }

  public updateUserSettings(params: SettingsFromApi | any) {
    const u = this.getUserObject();
    if (u) {
      const p = {};
      u.settings = {...params, id: params._id || params.id};
    }
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem('immo.user', JSON.stringify(u));
    }
  }
}
