import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import * as I18N_EN_FILE from '../../assets/i18n/en.json';
import * as I18N_FR_FILE from '../../assets/i18n/fr.json';
import {isPlatformBrowser} from '@angular/common';

export enum Language {
  EN = 'en',
  FR = 'fr'
}

export const SupportedLanguage = [Language.EN, Language.FR];

export function setupI18nFactory(service: I18nService): Function {
  return () => service.setUp();
}

@Injectable()
export class I18nService {
  data: any = {};
  currentLang =  SupportedLanguage[0];
  constructor(
    @Inject(PLATFORM_ID) private platformId: any
  ) {
  }

  flattenObject(ob: any) {
    const toReturn = {};
    for (const i in ob) {
      if (!ob.hasOwnProperty(i)) {
        continue;
      }
      if (typeof ob[i] === 'object' && ob[i] !== null) {
        const flatObject = this.flattenObject(ob[i]);
        for (const x in flatObject) {
          if (!flatObject.hasOwnProperty(x)) {
            continue;
          }
          toReturn[i + '.' + x] = flatObject[x];
        }
      } else {
        toReturn[i] = ob[i];
      }
    }
    return toReturn;
  }

  setUp(): Promise<{}> {
    return new Promise<{}>((resolve) => {
      if (isPlatformBrowser(this.platformId)) {
        this.currentLang = (localStorage.getItem('immo.lang') || Language.FR) as Language;
      }

      switch (this.currentLang) {
        case Language.EN:
          // @ts-ignore
          this.data = this.flattenObject(I18N_EN_FILE.default);
          break;
        case Language.FR:
          // @ts-ignore
          this.data = this.flattenObject(I18N_FR_FILE.default);
          break;
        default:
          // @ts-ignore
          this.data = this.flattenObject(I18N_EN_FILE.default);
          return;
      }
      // console.log('<-> ', this.data);
      resolve(this.data);
    });
  }
}
