import {Injectable} from '@angular/core';
import { environment } from '~/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConversationsService {

  constructor(
      private httpClient: HttpClient
  ) { }

  public createConversation(sender: string, receiver: string): Observable<any> {
    return this.httpClient.post(
        `${environment.ApiBaseUrl}conversations`,
        {
            sender,
            receiver,
        }
    );
  }
}
