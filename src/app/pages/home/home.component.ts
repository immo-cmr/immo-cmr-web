import {ChangeDetectorRef, Component, NgZone, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {select, Store} from '@ngrx/store';
import {AppState} from '~/app/redux/reducers';
import {FetchMoreProducts, FetchProducts, FetchRecentProducts, ProductsActionTypes} from '~/app/redux/actions/products.actions';
import {Product} from '~/app/models/product';
import {BaseComponent} from '~/app/components/base.component';
import * as fromProducts from '~/app/redux/reducers/products.reducer';
import * as fromLayout from '~/app/redux/reducers/layout.reducer';
import * as fromSearch from '~/app/redux/reducers/searches.reducer';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {ActivatedRoute, Router} from '@angular/router';
import {Search, SearchFilter} from '~/app/models/search';
import {Actions, ofType} from '@ngrx/effects';
import {RunMoreSearch, RunSearch} from '~/app/redux/actions/search.actions';

export enum PropertiesViewModeEnum {
  LIST = 'list',
  GRID = 'grid'
}

export class FilterOption {
  id: string;
  title: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit {

  products: Product[];
  recentProducts: Product[];
  layout: fromLayout.State;
  viewMode: PropertiesViewModeEnum;
  propertyViewModes = PropertiesViewModeEnum;
  page = 1;
  pages = null;
  total = 0;
  public listFilters: FilterOption[];
  public activeListFilter: FilterOption;
  searchQueryParams: {};
  isSearch = false;
  currentSearch: Search;

  constructor(
    private titleService: Title,
    private store: Store<AppState>,
    private i18n: I18nPipe,
    private route: ActivatedRoute,
    private router: Router,
    private dispatcher: Actions,
    private zone: NgZone,
    private cd: ChangeDetectorRef,
  ) {
    super();
    this.viewMode = PropertiesViewModeEnum.LIST;
    this.subscriptions.add(
      this.store.pipe(
        select(fromLayout.getLayoutFeatureState)
      ).subscribe((res) => (this.layout = res))
    );

    this.subscriptions.add(
      this.store.pipe(
        select(fromProducts.getProductsFeatureState)
      ).subscribe((res) => {
        if (!this.layout.isSearching) {
          this.products = res.entities;
          // this.page = res.page;
          this.pages = res.pages;
          this.total = res.total;
        }
        this.recentProducts = res.recent;
      })
    );

    this.subscriptions.add(
      this.store.pipe(
        select(fromSearch.getSearchesFeatureState)
      ).subscribe((res) => {
        if (this.isSearch || this.layout && this.layout.isSearching) {
          this.products = res.entities;
          // this.page = res.page;
          this.pages = res.pages;
          this.total = res.total;
        }
      })
    );

    this.subscriptions.add(
      this.dispatcher.pipe(
        ofType(ProductsActionTypes.FetchMoreProductSuccessAction)
      ).subscribe(res => {
        const pos = window.pageYOffset;
        if (pos > 0) {
          // window.scrollTo(0, pos - 2000); // how far to scroll on each step
        }
      })
    );
  }

  ngOnInit() {
    this.titleService.setTitle('Home');
    this.store.dispatch(
      new FetchRecentProducts()
    );
    if (!this.products || !this.products.length) {
      this.store.dispatch(
        new FetchProducts(+this.route.snapshot.queryParams.page || 1)
      );
      this.route.queryParams.subscribe(params => {
        /* if (params.page) {
           this.page = +params.page;
         }*/
        let isSearch = false;
        const q = {};
        if (params.nbRooms) {
          isSearch = true;
          Object.defineProperty(q, 'nb_rooms', { value: params.nbRooms, writable: true });
        }
        if (params.transactionType) {
          isSearch = true;
          Object.defineProperty(q, 'transaction_type', { value: params.transactionType, writable: true });
        }
        if (params.productType) {
          isSearch = true;
          Object.defineProperty(q, 'type', { value: [params.productType], writable: true });
        }
        if (params.priceMax) {
          isSearch = true;
          Object.defineProperty(q, 'price_max', { value: +params.priceMax, writable: true });
        }
        if (params.priceMin) {
          isSearch = true;
          Object.defineProperty(q, 'price_min', { value: +params.priceMin, writable: true });
        }
        if (params.place) {
          isSearch = true;
          Object.defineProperty(q, 'locations', { value: [params.place], writable: true });
        }

        this.searchQueryParams = params;
        // this.cd.detectChanges();
        // console.log(params);
        // console.log(new Search(q), params, q);
        this.isSearch = isSearch;
        this.currentSearch = new Search(q);
        if (isSearch) {
          this.store.dispatch(
            new RunSearch(this.currentSearch)
          );
        } else {
        }
      });
    }

    this.listFilters = [
      {id: 'updatedAt.desc', title: this.i18n.transform('filters.dateAsc')},
      {id: 'updatedAt.asc', title: this.i18n.transform('filters.dateDesc')},
      {id: 'price.asc', title: this.i18n.transform('filters.priceAsc')},
      {id: 'price.desc', title: this.i18n.transform('filters.priceDesc')},
    ];
    this.activeListFilter = this.listFilters[0];

    // console.log(this.i18n.transform('menu.home'));
  }

  onFilterSelect(filter: FilterOption) {
    this.activeListFilter = filter;
    const split = filter.id.split('.');
    this.store.dispatch(
      new FetchProducts(1, new SearchFilter(split[0], split[1]))
    );
    // this.resetNavigation();
  }

  handleNavigationChanged(event: { page: number }) {
    this.page = event.page;
    this.store.dispatch(
      new FetchMoreProducts(event.page)
    );
    // this.resetNavigation(event.page);
  }

  /*private resetNavigation(page = 1) {
    this.page = page;
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {
          // ...this.route.queryParams,
          page
        },
        queryParamsHandling: 'merge', // remove to replace all query params by provided
      });
  }*/

  loadMore() {
    this.page = this.page + 1;
    if (this.isSearch || this.layout && this.layout.isSearching)  {
      this.store.dispatch(
        new RunMoreSearch(this.currentSearch, this.page)
      );
    } else {
      this.store.dispatch(
        new FetchMoreProducts(this.page)
      );
    }
    // this.resetNavigation(this.page);
  }
}
