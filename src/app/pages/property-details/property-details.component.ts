import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ProductsService} from '~/app/services/products.service';
import {ActivatedRoute} from '@angular/router';
import {Product, ProductFromApi} from '~/app/models/product';

@Component({
  selector: 'app-property-details',
  templateUrl: './property-details.component.html',
  styleUrls: ['./property-details.component.scss']
})
export class PropertyDetailsComponent implements OnInit {

  fetchingDetails = true;
  property: Product;
  similarProperties: Product[];

  constructor(
    private titleService: Title,
    private productsService: ProductsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Loading ...');
    this.route.paramMap.subscribe(route => {
      this.productsService.fetchProductDetails(/*this.route.snapshot.params.id*/ route.get('id'), true)
        .subscribe((res: {
          property: ProductFromApi,
          similar: ProductFromApi[]
        }) => {
          setTimeout(() => {
            this.fetchingDetails = false;
          });
          this.property = new Product(res.property);
          this.titleService.setTitle(this.property.title);
          this.similarProperties = res.similar.map(s => (new Product(s)));
        }, error => {
          this.fetchingDetails = false;
          console.log(error);
        });
    });
  }

}
