import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {initPlaceSearchInput, productCommoditiesHelper, productTransactionTypesHelper, productTypesHelper} from '~/app/helpers/helpers';
import {NgxDropzoneChangeEvent} from 'ngx-dropzone/lib/ngx-dropzone/ngx-dropzone.component';
import {ProductsService} from '~/app/services/products.service';
import * as fromAuth from '~/app/redux/reducers/auth.reducer';
import {Observable} from 'rxjs';
import {BaseComponent} from '~/app/components/base.component';
import {select, Store} from '@ngrx/store';
import {map} from 'rxjs/internal/operators';
import {AppState} from '~/app/redux/reducers';

@Component({
  selector: 'app-create-property',
  templateUrl: './create-property.component.html',
  styleUrls: ['./create-property.component.scss']
})
export class CreatePropertyComponent extends BaseComponent implements OnInit {
  @ViewChild('cityInput', { static: true }) cityInput: ElementRef;
  public isLoggedIn$: Observable<boolean>;

  propertyInfosFormGroup: FormGroup;
  propertyOptionsFormGroup: FormGroup;
  propertyLocationFormGroup: FormGroup;
  isEditable = true;
  nbChoices = ['1', '2', '3', '4', '5', '5+'];
  propertyTypes: { id: string, title: string }[] = [];
  propertyCommodities: { id: string, title: string }[] = [];
  propertyTransactionTypes: { id: string, title: string }[] = [];
  images = [];
  nbChoicesForms = [];
  today = new Date();
  isSaving = false;
  saveError: boolean;
  statusMessage: string;

  constructor(
    private _formBuilder: FormBuilder,
    private i18n: I18nPipe,
    private productService: ProductsService,
    private store: Store<AppState>
  ) {
    super();
    this.nbChoicesForms = [
      { id: 'nb_rooms', title: 'Nb Rooms' },
      { id: 'nb_garages', title: 'Nb Garages' },
      { id: 'nb_floors', title: 'Nb Floors' },
      { id: 'nb_bating_rooms', title: 'Nb Baiting rooms' },
      { id: 'nb_terraces', title: 'Nb terraces' },
    ];
    this.isLoggedIn$ = this.store.pipe(
      select(fromAuth.getAuthFeatureState),
      map(user => {
        if (user.user) {
          console.log(user);
          this.initPlaceAutocompleteInput();
        }
        return user.user !== null;
      })
    );
  }

  ngOnInit() {
    console.log('--->', this.cityInput);
    this.saveError = false;
    this.statusMessage = undefined;
    this.propertyTypes = productTypesHelper(this.i18n);
    this.propertyCommodities = productCommoditiesHelper(this.i18n);
    this.propertyTransactionTypes = productTransactionTypesHelper(this.i18n);
    this.propertyInfosFormGroup = this._formBuilder.group({
      title: ['', Validators.required],
      reference: [''],
      construction_year: [''],
      description: ['', Validators.required],
      price: ['0.0', Validators.required],
      transaction_type: ['sell', Validators.required],
    });
    this.propertyOptionsFormGroup = this._formBuilder.group({
      nb_rooms: [null],
      nb_garages: [null],
      nb_floors: [null],
      nb_bating_rooms: [null],
      reference: [null],
      construction_year: [null],
      surface: [null],
      nb_terraces: [null],
      type: this._formBuilder.group(this._FormControls(this.propertyTypes)),
      commodities: this._formBuilder.group(this._FormControls(this.propertyCommodities)),
    });
    this.propertyLocationFormGroup = this._formBuilder.group({
      city: ['', Validators.required],
      longitude: ['', Validators.required],
      latitude: ['', Validators.required],
    });
  }

  private initPlaceAutocompleteInput() {
    console.log(this.cityInput);
    if (this.cityInput) {
      const placesAutocomplete = initPlaceSearchInput(this.cityInput.nativeElement);
      if (placesAutocomplete) {
        placesAutocomplete.on('change', (e) => {
          if (e.suggestion && e.suggestion.latlng) {
            // console.log(e.suggestion.latlng);
            this.propertyLocationFormGroup.patchValue({
              longitude: e.suggestion.latlng.lng,
              latitude: e.suggestion.latlng.lat,
            });
          }
        });
      }
    }
  }

  private _FormControls(data) {
    const obj = {};
    data.forEach(p => {
      obj[p.id] = new FormControl(false);
      return obj;
    });
    return obj;
  }

  onImagesDropped(event: NgxDropzoneChangeEvent) {
    // console.log(event.addedFiles);
    if (event.addedFiles) {
      this.images.push(...event.addedFiles);
    }
  }

  onImageRemove(f: any) {
    console.log(f);
  }

  publish() {
    this.isSaving = true;
    const commodities = [];
    const types = [];
    const options = [];
    for (const o in this.propertyOptionsFormGroup.value.commodities) {
      if (this.propertyOptionsFormGroup.value.commodities[o]) {
        commodities.push(o);
      }
    }

    for (const o in this.propertyOptionsFormGroup.value.type) {
      if (this.propertyOptionsFormGroup.value.type[o]) {
        types.push(o);
      }
    }

    for (const o in this.propertyOptionsFormGroup.value) {
      if (o !== 'type' && o !== 'commodities' && this.propertyOptionsFormGroup.value[o] !== null) {
        options[o] = this.propertyOptionsFormGroup.value[o];
      }
    }
    const p = {
      ...this.propertyInfosFormGroup.value,
      commodities,
      images: [''],
      location: this.propertyLocationFormGroup.value,
      options: {
        ...options,
        reference: this.propertyInfosFormGroup.value.reference,
        construction_year: this.propertyInfosFormGroup.value.construction_year,
      },
      type: types,
    };
    delete p.construction_year;
    delete p.reference;
    const fd = new FormData();
    this.images.forEach(img => {
      // console.log(img);
      fd.append('images[]', img, img.name);
    });
    fd.append('property', JSON.stringify(p));
    this.productService.store(fd).subscribe(res => {
      this.isSaving = false;
      this.saveError = false;
      this.statusMessage = this.i18n.transform('alert.savePropertySuccess');
    }, error => {
      console.log(error);
      this.isSaving = false;
      this.saveError = true;
      this.statusMessage = this.i18n.transform('alert.savePropertyError');
    });
  }
}
