import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {PropertyDetailsComponent} from './pages/property-details/property-details.component';
import {CreatePropertyComponent} from '~/app/pages/create-property/create-property.component';
import {ChatComponent} from '~/app/pages/chat/chat.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
  }, {
    path: ':id/details', component: PropertyDetailsComponent,
  }, {
    path: 'property/add', component: CreatePropertyComponent,
  }, {
    path: 'conversations', component: ChatComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
