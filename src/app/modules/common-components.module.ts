import {APP_INITIALIZER, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatBadgeModule,
  MatButtonModule,
  MatCardModule, MatCheckboxModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressSpinnerModule, MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule, MatSlideToggleModule, MatStepperModule,
  MatTabsModule,
  MatToolbarModule,
} from '@angular/material';
import {AppHeaderComponent} from '../components/layout/app-header/app-header.component';
import {HeaderCarouselComponent} from '../components/header-carousel/header-carousel.component';
import {
  ButtonsModule,
  CardsModule,
  CarouselModule,
  IconsModule,
  InputsModule,
  InputUtilitiesModule,
  ModalModule
} from 'angular-bootstrap-md';
import {SearchComponent} from '../components/search/search.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RecentPostsComponent} from '../components/recent-posts/recent-posts.component';
import {PostCardItemComponent} from '../components/post-card-item/post-card-item.component';
import {AppFooterComponent} from '../components/layout/app-footer/app-footer.component';
import {HomeComponent} from '../pages/home/home.component';
import {PostCategoryItemComponent} from '../components/post-category-item/post-category-item.component';
import {SearchBarComponent} from '../components/search-bar/search-bar.component';
import {RecentPostItemComponent} from '../components/recent-post-item/recent-post-item.component';
import {PostItemComponent} from '../components/post-item/post-item.component';
import {PropertyDetailsComponent} from '../pages/property-details/property-details.component';
import {AppRoutingModule} from '../app-routing.module';
import {GooglePlaceService} from '~/app/services/google-place.service';
import {LocalstorageService} from '~/app/services/localstorage.service';
import {ProductsService} from '~/app/services/products.service';
import {MomentPipe} from '~/app/pipes/moment.pipe';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {AuthService} from '~/app/services/auth.service';
import {PusherService} from '~/app/services/pusher.service';
import {ConversationsService} from '~/app/services/conversations.service';
import {I18nService, setupI18nFactory} from '~/app/services/i18n.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpInterceptorService} from '~/app/services/http-interceptor.service';
import {AuthComponent} from '../components/auth/auth.component';
import {GooglePlaceModule} from 'ngx-google-places-autocomplete';
import { CreatePropertyComponent } from '../pages/create-property/create-property.component';
import {NgxDropzoneModule} from 'ngx-dropzone';
import { RequestAuthComponent } from '../components/layout/request-auth/request-auth.component';
import { ChatComponent } from '../pages/chat/chat.component';

const declarations = [
  HeaderCarouselComponent,
  AppHeaderComponent,
  SearchComponent,
  RecentPostsComponent,
  PostCardItemComponent,
  AppFooterComponent,
  HomeComponent,
  PostCategoryItemComponent,
  SearchBarComponent,
  RecentPostItemComponent,
  PostItemComponent,
  PropertyDetailsComponent,
  MomentPipe,
  I18nPipe,
  AuthComponent,
  CreatePropertyComponent,
  RequestAuthComponent,
  ChatComponent,
];

const modules = [
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatIconModule,
  MatRippleModule,
  MatTabsModule,
  CarouselModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  ReactiveFormsModule,
  IconsModule,
  CardsModule,
  InputsModule,
  FormsModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  InputUtilitiesModule,
  ButtonsModule,
  MatStepperModule,
  MatSlideToggleModule,
  MatCheckboxModule,
  NgxDropzoneModule,
  MatRadioModule,
  MatBadgeModule
];

@NgModule({
  declarations: [
    ...declarations,
  ],
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    GooglePlaceModule,
    ...modules,
    AppRoutingModule
  ],
  exports: [
    ...modules,
    ...declarations,
    AppHeaderComponent,
  ],
  providers: [
    I18nService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupI18nFactory,
      deps: [
        I18nService
      ],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    GooglePlaceService,
    LocalstorageService,
    I18nPipe,
    ProductsService,
    MomentPipe,
    AuthService,
    PusherService,
    ConversationsService
  ],
  entryComponents: [
    AuthComponent
  ]
})
export class CommonComponentsModule { }
