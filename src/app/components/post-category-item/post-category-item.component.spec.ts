import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCategoryItemComponent } from './post-category-item.component';

describe('PostCategoryItemComponent', () => {
  let component: PostCategoryItemComponent;
  let fixture: ComponentFixture<PostCategoryItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostCategoryItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCategoryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
