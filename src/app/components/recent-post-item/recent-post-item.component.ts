import {Component, Input, OnInit} from '@angular/core';
import {Product} from '~/app/models/product';

@Component({
  selector: 'app-recent-post-item',
  templateUrl: './recent-post-item.component.html',
  styleUrls: ['./recent-post-item.component.scss']
})
export class RecentPostItemComponent implements OnInit {
  @Input() property: Product;

  constructor() { }

  ngOnInit() {
  }

}
