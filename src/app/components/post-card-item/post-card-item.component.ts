import {Component, Input, OnInit} from '@angular/core';
import {Product} from '~/app/models/product';

@Component({
  selector: 'app-post-card-item',
  templateUrl: './post-card-item.component.html',
  styleUrls: ['./post-card-item.component.scss']
})
export class PostCardItemComponent implements OnInit {

  @Input() public product: Product;

  constructor() { }

  ngOnInit() {
  }

}
