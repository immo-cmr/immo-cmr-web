import {Component, Input, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {Product} from '~/app/models/product';

@Component({
  selector: 'app-recent-posts',
  templateUrl: './recent-posts.component.html',
  styleUrls: ['./recent-posts.component.scss']
})
export class RecentPostsComponent implements OnInit {

  @Input() public properties: Product[];

  constructor(
    private title: Title,
    private meta: Meta
  ) { }

  ngOnInit() {
    // this.title.setTitle('Cool title here dude');
    // this.meta.addTag({ url: 'google.fr', content: 'style/css' }, true);
  }
}
