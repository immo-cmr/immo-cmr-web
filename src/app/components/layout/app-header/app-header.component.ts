import { Component, OnInit } from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppState} from '~/app/redux/reducers';
import {ShowAuthModal} from '~/app/redux/actions/auth.actions';
import * as fromAuth from '~/app/redux/reducers/auth.reducer';
import {Observable} from 'rxjs';
import {BaseComponent} from '~/app/components/base.component';
import {map} from 'rxjs/internal/operators';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent extends BaseComponent implements OnInit {
  public isLoggedIn$: Observable<boolean>;

  constructor(
    private store: Store<AppState>
  ) {
    super();
    this.isLoggedIn$ = this.store.pipe(
      select(fromAuth.getAuthFeatureState),
      map(user => {
        return user.user !== null;
      })
    );
  }

  ngOnInit() {
  }

  showAuthForm() {
    this.store.dispatch(
      new ShowAuthModal()
    );
  }
}
