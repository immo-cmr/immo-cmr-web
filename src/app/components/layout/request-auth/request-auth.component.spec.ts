import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestAuthComponent } from './request-auth.component';

describe('RequestAuthComponent', () => {
  let component: RequestAuthComponent;
  let fixture: ComponentFixture<RequestAuthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestAuthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
