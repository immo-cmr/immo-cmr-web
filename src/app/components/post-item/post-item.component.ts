import {Component, Input, OnInit} from '@angular/core';
import {Product} from '~/app/models/product';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.scss']
})
export class PostItemComponent implements OnInit {

  @Input() public product: Product;

  constructor() { }

  ngOnInit() {
  }

}
