import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {AppState} from '~/app/redux/reducers';
import * as fromLayout from '~/app/redux/reducers/layout.reducer';
import {BaseComponent} from '~/app/components/base.component';
import {Actions, ofType} from '@ngrx/effects';
import {AuthActionTypes, Login, LoginFailure, LoginSuccess} from '~/app/redux/actions/auth.actions';
import {ModalDirective} from 'angular-bootstrap-md';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent extends BaseComponent implements OnInit {
  @ViewChild('loginFrame', { static: true }) loginFrame: ModalDirective;
  @ViewChild('registerFrame', { static: true }) registerFrame: ModalDirective;
  loginForm: FormGroup;
  registerForm: FormGroup;
  isProf = false;
  layout: fromLayout.State;
  loginFailed = undefined;

  constructor(
    private store: Store<AppState>,
    private dispatcher: Actions
  ) {
    super();
    this.subscriptions.add(
      this.dispatcher.pipe(
        ofType(AuthActionTypes.ShowAuthForm)
      ).subscribe(res => {
        this.loginFrame.show();
      })
    );

    this.subscriptions.add(
      this.dispatcher.pipe(
        ofType(AuthActionTypes.LoginFailureAction)
      ).subscribe((res: LoginFailure) => {
        this.loginFailed = res.message;
      })
    );

    this.subscriptions.add(
      this.dispatcher.pipe(
        ofType(AuthActionTypes.LoginSuccessAction)
      ).subscribe((res: LoginSuccess) => {
        this.loginFrame.hide();
      })
    );

    this.subscriptions.add(
      this.store.pipe(
        select(fromLayout.getLayoutFeatureState)
      ).subscribe(layout => (this.layout = layout))
    );
  }

  ngOnInit() {
    this.loginFailed = undefined;
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.email),
      password: new FormControl('', Validators.required)
    });

    this.registerForm = new FormGroup({
      signupFormModalName: new FormControl('', Validators.required),
      signupFormModalEmail: new FormControl('', Validators.email),
      signupFormModalPassword: new FormControl('', Validators.required),
    });
  }

  get loginFormModalEmail() {
    return this.loginForm.get('email');
  }

  get loginFormModalPassword() {
    return this.loginForm.get('password');
  }

  get signupFormModalName() {
    return this.registerForm.get('signupFormModalName');
  }

  get signupFormModalEmail() {
    return this.registerForm.get('signupFormModalEmail');
  }

  get signupFormModalPassword() {
    return this.registerForm.get('signupFormModalPassword');
  }

  public showRegister() {
    this.loginFailed = undefined;
    this.loginFrame.hide();
    this.registerFrame.show();
  }

  public showLogin() {
    this.registerFrame.hide();
    this.loginFrame.show();
  }

  login() {
    this.loginFailed = undefined;
    if (this.loginForm.valid) {
      /*this.loginForm.setErrors({
        notUnique: true
      })*/
      this.store.dispatch(
        new Login({
          isLogin: true,
          ...this.loginForm.value
        })
      );
    }
  }
}
