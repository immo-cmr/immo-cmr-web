import {Subscription} from 'rxjs';
import {OnDestroy} from '@angular/core';

// import {AuthComponent} from '~/app/components/auth/auth.component';

export abstract class BaseComponent implements OnDestroy {
  protected subscriptions = new Subscription();

  protected constructor() {
  }

  public ngOnDestroy() {
    // console.log('UNSUBSCRIBED ', this.subscriptions);
    this.subscriptions.unsubscribe();
  }

  /*public requestLogin(modalService: ModalDialogService, vcRef: ViewContainerRef, actionsToRunAfterLogin?: Action[]) {
      const options: ModalDialogOptions = {
          viewContainerRef: vcRef,
          context: {
              actions: actionsToRunAfterLogin || null
          },
          fullscreen: false,
          animated: true,
      };

      modalService.showModal(AuthComponent, options)
          .then((result: string) => {
              // console.log(' <-> ', result);
          });
  }*/
}
