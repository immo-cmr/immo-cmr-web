import {Component, ElementRef, Input, NgZone, OnInit, ViewChild} from '@angular/core';
import {GooglePlaceService} from '~/app/services/google-place.service';
import {ComponentRestrictions} from 'ngx-google-places-autocomplete/objects/options/componentRestrictions';
import {Address} from 'ngx-google-places-autocomplete/objects/address';
import {Search} from '~/app/models/search';
import {Router} from '@angular/router';
import {I18nPipe} from '~/app/pipes/i18n.pipe';
import {initPlaceSearchInput, productTransactionTypesHelper, productTypesHelper} from '~/app/helpers/helpers';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  @ViewChild('cityInput', { static: true }) cityInput: ElementRef;
  displayAdvancedForm = true;
  options: any;

  @Input() public routeQueryParams: {};

  public searchForm: {
    place?: string;
    priceMin?: number;
    priceMax?: number;
    transactionType?: string;
    productType?: string;
    nbRooms?: string;
  };

  propertyTypes = [];
  transactionTypes = [];
  nbRooms = ['1', '2', '3', '4', '5', '5+'];

  constructor(
    private googleAutoCompleteService: GooglePlaceService,
    private router: Router,
    private i18n: I18nPipe
  ) { }

  ngOnInit() {
    this.options = {
      types: [],
      componentRestrictions: { country: 'CM' }
    };
    this.searchForm = {
      place: '',
      priceMax: null,
      priceMin: null,
      productType: '',
      transactionType: '',
      nbRooms: '',
      ...this.routeQueryParams
    };
    this.propertyTypes = productTypesHelper(this.i18n);

    this.transactionTypes = productTransactionTypesHelper(this.i18n);

    const placesAutocomplete = initPlaceSearchInput(this.cityInput.nativeElement);
  }

  public search(q: string) {
    /*console.log(q);
    if (q.trim().length > 2) {
      this.googleAutoCompleteService.search(q.trim())
        .subscribe(res => {
          console.log(res);
        });
    }*/
  }

  handleAddressChange(event: Address) {
    console.log(event);
  }

  doSearch() {
    this.router.navigate([], {
      queryParams: {
        ...this.searchForm
      },
      queryParamsHandling: 'merge'
    });
  }
}
