// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // ApiBaseUrl: 'http://localhost:3000/',
  ApiBaseUrl: 'https://immo-api.serveo.net/',
  pusherKey: 'fa16f79859c2012a3e47',
  pusherCluster: 'mt1',
  googleAutoCompleteKey: 'AIzaSyAvQxve1og8rL0o4OiJ3NYxa6-tgUYOiNY',
  AlgoliaAPIId: 'plTFMEE66SYU',
  AlgoliaAPIKey: '81e92cc3219330942ddc8a409c079de1'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
